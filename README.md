<h1 align=center>lyrics - scrape AZlyrics from your terminal</h1>
<p align=center><img alt='screenshot' src='screenshot.png' /></p>

**lyrics** is a tool for fetching the lyrics of a song from [AZLyrics](https://azlyrics.com/) and viewing it in your terminal.

## How to use?

You can search for lyrics by passing your search text as a string of arguments to **lyrics**. You do not need to enclose the search text with quotes, because the arguments will be concatenated before the search query is made.

For example, to find the lyrics of Fighter by Jack Stauber, you can run:

    lyrics fighter jack stauber

Any search text that works in AZLyrics' online search feature will work when passed to **lyrics**.

If a search yields multiple results, the user will be prompted to select one of the results before fetching its lyrics. If a search yields only one result, the lyrics of the requested song will be fetched. If a search yields no results, the program will notify the user and exit.

You can use the `-f` or `--first` flag to print the first search result, no matter how many results the search yields. This argument must be the first argument passed to the script, or it will be treated as part of the search query.

Example:

    lyrics -f without me

will show the lyrics of Without Me by Eminem because it's the first result, despite there being a lot of other ones.

This can be useful when constructing an automated way of fetching song lyrics. For example, the following one-liner will attempt to print the lyrics of the currently playing song in Cmus.

    cmus-remote -Q | awk -F / '/file/ {print $NF}' | awk -F . '{$NF=""; print $0}' | xargs lyrics --first

the output of **lyrics** can be redirected to a file or piped to another program. (Colorcodes in the lyrics are only added if stdout is a terminal.)

    lyrics -f linoleum nofx > lyrics.txt

## Important notes

If you get a bunch of "Found no results" errors for queries that should actually yield results, go to the [AZLyrics](//azlyrics.com) site in your browser. They are probably going to give you a captcha to fill out.

This script works as of 2022-07-22, but AZLyrics is probably going to break it again. Feel free to submit an issue or a PR if you can think of a fix.

## How to install

**Requirements:**

* `python3`
* `lynx`
* `curl`
* Basic coreutils like grep, sed, cut, etc.

To install **lyrics**, clone this git repository and copy the script somewhere in your **$PATH**.

    git clone https://git.spitemim.xyz/lyrics
    cd lyrics
    sudo cp lyrics /usr/local/bin

If your `~/.local/bin` is in your path, you can just do:

    cp lyrics $HOME/.local/bin

## License

**lyrics** is free software, licensed under the GNU GPLv3.0. See the [LICENSE](LICENSE).
